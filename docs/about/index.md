# About me

![](../images/jcontreras2023.jpg)

Hi, I'm Jose Contreras. I am a teacher in the area of Mechanics in Tecsup Sur, resident in Arequipa, Perú, my career line is Design, with studies in Project Management, Mechanical Design and Design of Elements and Mechanisms.

Visit this website to see my work!

## My background

I was born in a beautiful city called Arequipa, in southern Peru.   It is the second largest city in the country.
The climate is sunny most of the year, with many touristic places and a great gastronomy represented in typical dishes.

## Previous work

In the courses I teach, I motivate students to develop projects in which they integrate the knowledge acquired in their previous and current courses, with the purpose of strengthening and improving their understanding by applying their knowledge on real machines and equipment.

### Project A

This is an image of a project developed with students:

Hydraulic press for vulcanizing and curing of conveyor belts

![](../images/week01/prensa.jpg)
![](../images/week01/prensa01.jpg)
![](../images/week01/prensa02.jpg)
